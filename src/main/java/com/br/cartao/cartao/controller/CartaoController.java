package com.br.cartao.cartao.controller;

import com.br.cartao.cartao.model.Cartao;
import com.br.cartao.cartao.model.dto.create.CreateCartaoRequest;
import com.br.cartao.cartao.model.dto.create.CreateCartaoResponse;
import com.br.cartao.cartao.model.dto.get.GetCartaoResponse;
import com.br.cartao.cartao.model.dto.update.UpdateCartaoRequest;
import com.br.cartao.cartao.model.dto.update.UpdateCartaoResponse;
import com.br.cartao.cartao.model.mapper.CartaoMapper;
import com.br.cartao.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartao")
public class CartaoController {
    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCartaoResponse create(@RequestBody CreateCartaoRequest createCartaoRequest) {
        Cartao cartao = cartaoMapper.fromCreateRequest(createCartaoRequest);

        cartao = cartaoService.create(cartao);

        return CartaoMapper.toCreateResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public UpdateCartaoResponse update(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = cartaoMapper.fromUpdateRequest(updateCartaoRequest);
        cartao.setNumero(numero);

        cartao = cartaoService.update(cartao);
        return CartaoMapper.toUpdateResponse(cartao);
    }

    @GetMapping("/{numero}")
    public GetCartaoResponse getByNumero(@PathVariable String numero) {
        Cartao byNumero = cartaoService.getByNumero(numero);

        return CartaoMapper.toGetResponse(byNumero);
    }

    @GetMapping("/buscarporid/{id}")
    public GetCartaoResponse getById(@PathVariable long id) {
        Cartao byNumero = cartaoService.getById(id);

        return CartaoMapper.toGetResponse(byNumero);
    }

}
