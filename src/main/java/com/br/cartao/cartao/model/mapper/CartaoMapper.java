package com.br.cartao.cartao.model.mapper;

import com.br.cartao.cartao.client.ClienteClient;
import com.br.cartao.cartao.client.ClienteDTO;
import com.br.cartao.cartao.model.Cartao;
import com.br.cartao.cartao.model.dto.create.CreateCartaoRequest;
import com.br.cartao.cartao.model.dto.create.CreateCartaoResponse;
import com.br.cartao.cartao.model.dto.get.GetCartaoResponse;
import com.br.cartao.cartao.model.dto.update.UpdateCartaoRequest;
import com.br.cartao.cartao.model.dto.update.UpdateCartaoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    @Autowired
    private ClienteClient clienteClient;


    public Cartao fromCreateRequest(CreateCartaoRequest cartaoCreateRequest) {
        ClienteDTO clienteDTO = clienteClient.getById(cartaoCreateRequest.getClienteId());

        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoCreateRequest.getNumero());

        cartao.setClienteId(clienteDTO.getId());
        return cartao;
    }

    public static CreateCartaoResponse toCreateResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumero(cartao.getNumero());
        createCartaoResponse.setClienteId(cartao.getClienteId());
        createCartaoResponse.setAtivo(cartao.getAtivo());

        return createCartaoResponse;
    }

    public static Cartao fromUpdateRequest(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(updateCartaoRequest.getAtivo());
        return cartao;
    }

    public static UpdateCartaoResponse toUpdateResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumero(cartao.getNumero());
        updateCartaoResponse.setClienteId(cartao.getClienteId());
        updateCartaoResponse.setAtivo(cartao.getAtivo());

        return updateCartaoResponse;
    }

    public static GetCartaoResponse toGetResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumero(cartao.getNumero());
        getCartaoResponse.setClienteId(cartao.getClienteId());

        return getCartaoResponse;
    }
}
